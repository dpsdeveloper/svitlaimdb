# README #

* Feature List: The screen should show a list of upcoming movies in List. Each row of List should be consisting of following information about movie
* 1. Movie title
* 2. Release date
* 3. Popularity
* 4. Votes count
* 5. Movie Image
 
# Notes: #
* Upcoming movies - doesn't have popularity or votes fields, becasue they aren't yet been released.
* The date 2017-8-9 was selected as exemple for fetching movies.
* Project used custom downloading approach, based on Callable interface.
* Application has 3 screens (Lauch, Discover,Details)
* Launch screen is in charge of loading configuration from ImDB service.
* Discover screen  is in charge of loading and showing Movies. It shows all movies  inside RecyclerView. It doesn't contain calendar for selecting date release, but it may be added.
* Detail screen is in charge of showing movie simple details.
* Images are loads without 3-d party libraries, and they are not being cached that why blinking affect might appear.
* No using 3rd party libraries.
* POC doesn't use MVP or any other architecture pattern, because it might be interpreted as over engineering.
