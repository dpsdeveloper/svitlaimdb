package svitla.com.dpsimdb.internals;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import svitla.com.dpsimdb.activities.DetailsActivity;
import svitla.com.dpsimdb.activities.DiscoverMovieActivity;
import svitla.com.dpsimdb.models.Movie;

import static svitla.com.dpsimdb.internals.Constants.MOVIE_PARCELABLE_KEY;

/**
 * Class designed as facilitator of navigation logic.
 * <p>
 * Created by dps on 9/12/17.
 */
public class Navigator {

    private AppCompatActivity activity;

    public Navigator(AppCompatActivity activity) {

        this.activity = activity;
    }

    /**
     * Should be use for navigate to Discover Movie Activity.
     */
    public void showDiscoverScreen() {
        Intent intent = new Intent(activity, DiscoverMovieActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
        activity.finish();
    }

    /**
     * Should be use for navigate to Details Activity.
     *
     * @param movie - Movie object.
     */
    public void showDetailsScreen(Movie movie) {
        Intent intent = new Intent(activity, DetailsActivity.class);
        intent.putExtra(MOVIE_PARCELABLE_KEY, movie);
        activity.startActivity(intent);
    }

}
