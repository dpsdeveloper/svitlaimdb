package svitla.com.dpsimdb.network;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

import svitla.com.dpsimdb.BuildConfig;
import svitla.com.dpsimdb.interfaces.ApiDateFormatter;
import svitla.com.dpsimdb.interfaces.Logger;

import static svitla.com.dpsimdb.internals.Constants.API_KEY;
import static svitla.com.dpsimdb.internals.Constants.BASE_URL;


/**
 * Class utils for Discover IMDB url creating.
 * <p>
 * Created by dps on 9/12/17.
 */
public class DiscoverUrlCreator {

    private static final String TAG = DiscoverUrlCreator.class.getCanonicalName();

    private static final String DISCOVER_PATH = "/discover/movie?";

    private static final String PRIMARY_RELEASE_PARAM = "primary_release_date.lte=%s";

    private static final String API_KEY_PARAM = "&" + API_KEY + "=" + BuildConfig.IMDB_API_KEY;

    public static final String SORT_BY_RELEASE_DATE_DESC = "&sort_by=release_date.desc";
    private static final String DISCOVER_TODAY_URL = BASE_URL + DISCOVER_PATH + PRIMARY_RELEASE_PARAM + API_KEY_PARAM + SORT_BY_RELEASE_DATE_DESC;

    private Logger logger;
    private ApiDateFormatter formatter;

    public DiscoverUrlCreator(Logger logger, ApiDateFormatter formatter) {
        this.logger = logger;
        this.formatter = formatter;
    }

    /**
     * Should be use for creating URL for fetching currently viewing movies.
     *
     * @param date - Date object
     * @return - URL for fetching movies from IMDB server.
     */
    public URL createCurrentlyShowingDiscoverURL(Date date) {
        try {
            String dateInSpecifiedFormat = formatter.format(date);
            return new URL(String.format(DISCOVER_TODAY_URL, dateInSpecifiedFormat));
        } catch (MalformedURLException e) {
            logger.warning(TAG, e.getMessage());
        }
        throw new IllegalArgumentException("Check param Date");
    }


}
