package svitla.com.dpsimdb.interfaces;

/**
 * Interface designed for facilitate passing android dependent classes over different part of bussines logic.
 * <p>
 * Created by dps on 9/12/17.
 */
public interface Logger {
    void error(String tag, String msg);

    void error(String tag, String msg, Throwable tr);

    void debug(String tag, String msg);

    void debug(String tag, String msg, Throwable tr);

    void info(String tag, String msg);

    void info(String tag, String msg, Throwable tr);

    void verbose(String tag, String msg);

    void verbose(String tag, String msg, Throwable tr);

    void warning(String tag, String msg);

    void warning(String tag, String msg, Throwable tr);
}
