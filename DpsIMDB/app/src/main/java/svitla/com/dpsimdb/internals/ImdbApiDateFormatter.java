package svitla.com.dpsimdb.internals;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import svitla.com.dpsimdb.interfaces.ApiDateFormatter;

/**
 * Created by dps on 9/12/17.
 */

public class ImdbApiDateFormatter implements ApiDateFormatter {

    static final String API_DATE_FORMAT = "yyyy-dd-MM";

    final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(API_DATE_FORMAT, Locale.getDefault());

    public ImdbApiDateFormatter() {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(Date date) {
        return DATE_FORMAT.format(date);
    }
}
