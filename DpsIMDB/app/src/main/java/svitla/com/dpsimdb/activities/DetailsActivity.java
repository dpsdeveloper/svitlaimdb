package svitla.com.dpsimdb.activities;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import svitla.com.dpsimdb.IMDBApplication;
import svitla.com.dpsimdb.R;
import svitla.com.dpsimdb.interfaces.Callback;
import svitla.com.dpsimdb.interfaces.Cancelable;
import svitla.com.dpsimdb.interfaces.ProgressIndicator;
import svitla.com.dpsimdb.internals.Constants;
import svitla.com.dpsimdb.internals.SizeAdjustable;
import svitla.com.dpsimdb.models.Movie;
import svitla.com.dpsimdb.network.BitmapFetcher;
import svitla.com.dpsimdb.views.ProgressIndicatorDispatcher;

public class DetailsActivity extends AppCompatActivity {
    private ImageView cover;
    private Movie movie;
    private Cancelable cancelable;
    private ProgressIndicator indicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getString(R.string.details_title));
        movie = getIntent().getParcelableExtra(Constants.MOVIE_PARCELABLE_KEY);
        TextView overview = (TextView) findViewById(R.id.overview);
        cover = (ImageView) findViewById(R.id.big_cover);
        overview.setText(movie.overview);
        indicator = new ProgressIndicatorDispatcher(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!movie.posterPath.isEmpty()) {
            indicator.showProgressIndicator();
            loadCover();
        }
    }

    /**
     * Might be use for load cover image of the movie.
     */
    private void loadCover() {
        cover.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                cover.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                int width = cover.getHeight();
                // will calculate suitable size
                String suitableSize = new SizeAdjustable().detectSuitableSize(IMDBApplication.configuration(), width);
                String finalURL = IMDBApplication.configuration().baseURL + suitableSize + movie.posterPath;
                cancelable = IMDBApplication.poolExecutor().enqueue(new BitmapFetcher(finalURL), new Callback<Bitmap>() {
                    @Override
                    public void onSuccess(Bitmap result) {
                        indicator.hideProgressIndicator();
                        cover.setImageBitmap(result);
                    }

                    @Override
                    public void onError(Throwable error) {
                        indicator.hideProgressIndicator();
                        Toast.makeText(DetailsActivity.this, "Something went wrong while image loading.", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (cancelable != null) {
            cancelable.cancel();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }
}
