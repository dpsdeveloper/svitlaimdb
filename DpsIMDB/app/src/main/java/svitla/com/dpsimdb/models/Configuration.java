package svitla.com.dpsimdb.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.JsonReader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * POJO class should represents server side API configuration.
 * <p>
 * Created by dps on 9/12/17.
 */
public class Configuration implements Parcelable {

    private static final String IMAGES = "images";
    private static final String IMAGE_BASE_URL = "base_url";
    private static final String POSTER_SIZES = "poster_sizes";
    public static final int CAPACITY = 10;

    public final String baseURL;
    public final List<String> supportedPosterSizes;

    public Configuration(JsonReader reader) throws IOException {
        String baseUrl = "";
        List<String> list = Collections.unmodifiableList(Collections.<String>emptyList());
        reader.beginObject();
        while (reader.hasNext()) {
            if (IMAGES.equalsIgnoreCase(reader.nextName())) {
                reader.beginObject();
                while (reader.hasNext()) {
                    String name = reader.nextName();
                    switch (name) {
                        case IMAGE_BASE_URL:
                            baseUrl = reader.nextString();
                            break;
                        case POSTER_SIZES:
                            reader.beginArray();
                            list = new ArrayList<>(CAPACITY);
                            while (reader.hasNext()) {
                                list.add(reader.nextString());
                            }
                            reader.endArray();
                            // safe collection wrapping
                            list = Collections.unmodifiableList(list);
                            break;
                        default:
                            reader.skipValue();
                            break;
                    }
                }
                reader.endObject();
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        this.baseURL = baseUrl;
        this.supportedPosterSizes = list;
    }

    protected Configuration(Parcel in) {
        baseURL = in.readString();
        supportedPosterSizes = in.createStringArrayList();
    }

    public static final Creator<Configuration> CREATOR = new Creator<Configuration>() {
        @Override
        public Configuration createFromParcel(Parcel in) {
            return new Configuration(in);
        }

        @Override
        public Configuration[] newArray(int size) {
            return new Configuration[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(baseURL);
        dest.writeStringList(supportedPosterSizes);
    }
}
