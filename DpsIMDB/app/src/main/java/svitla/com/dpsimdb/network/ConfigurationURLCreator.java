package svitla.com.dpsimdb.network;

import java.net.MalformedURLException;
import java.net.URL;

import svitla.com.dpsimdb.BuildConfig;
import svitla.com.dpsimdb.interfaces.Logger;

import static svitla.com.dpsimdb.internals.Constants.API_KEY;
import static svitla.com.dpsimdb.internals.Constants.BASE_URL;

/**
 * Class goal is to create url for getting configuration form IMDM server.
 * <p>
 * Created by dps on 9/12/17.
 */
public class ConfigurationURLCreator {

    private static final String TAG = ConfigurationURLCreator.class.getCanonicalName();

    private static final String CONFIGURATION_PATH = "/configuration";
    private static final String FINAL_URL = BASE_URL + CONFIGURATION_PATH + "?" + API_KEY + "=" + BuildConfig.IMDB_API_KEY;
    private Logger logger;

    public ConfigurationURLCreator(Logger logger) {

        this.logger = logger;
    }

    /**
     * Might be use for create URL for fetch configuration.
     *
     * @return - created URL object or throws Runtime exception
     */
    public URL createConfigurationURL() {
        try {
            return new URL(FINAL_URL);
        } catch (MalformedURLException e) {
            logger.warning(TAG, e.getMessage());
        }
        throw new IllegalArgumentException("Check param Date");

    }

}
