package svitla.com.dpsimdb;

import android.os.Handler;
import android.os.Looper;

import java.util.concurrent.Executors;

import svitla.com.dpsimdb.interfaces.Logger;
import svitla.com.dpsimdb.internals.AndroidBasedLogger;
import svitla.com.dpsimdb.internals.RequestExecutor;
import svitla.com.dpsimdb.models.Configuration;

/**
 * Created by dps on 9/12/17.
 */
public class IMDBApplication extends android.app.Application {

    protected static IMDBApplication self;

    RequestExecutor serialExecutor;
    RequestExecutor poolExecutor;
    Logger logger = new AndroidBasedLogger();
    Handler handler = new Handler(Looper.getMainLooper());
    Configuration configuration;

    @Override
    public void onCreate() {
        super.onCreate();
        self = this;
        serialExecutor = new RequestExecutor(Executors.newSingleThreadExecutor(), handler);
        poolExecutor = new RequestExecutor(Executors.newFixedThreadPool(10), handler);
    }

    public static RequestExecutor serialExecutor() {
        return self.serialExecutor;
    }

    public static RequestExecutor poolExecutor() {
        return self.poolExecutor;
    }

    /**
     * Saves for sharing across application.
     * Since Configuration is unmodifiable, it's safe to share it.
     *
     * @param configuration - Configuration object
     */
    public static void saveConfiguration(Configuration configuration) {
        self.configuration = configuration;
    }

    public static Configuration configuration() {
        return self.configuration;
    }

    public static Logger logger() {
        return self.logger;
    }
}
