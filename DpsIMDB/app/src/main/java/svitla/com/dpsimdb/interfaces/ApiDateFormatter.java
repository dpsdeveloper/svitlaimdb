package svitla.com.dpsimdb.interfaces;

import java.util.Date;

/**
 * Interface designed for encapsulate fomating procedure.
 * <p>
 * Created by dps on 9/12/17.
 */
public interface ApiDateFormatter {
    /**
     * Might be use for format date into specific String format.
     *
     * @param date - Date object
     * @return - String object in specific format
     */
    String format(Date date);

}
