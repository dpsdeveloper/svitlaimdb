package svitla.com.dpsimdb.network;

import android.util.JsonReader;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HttpsURLConnection;

import svitla.com.dpsimdb.interfaces.Logger;
import svitla.com.dpsimdb.models.Configuration;

/**
 * Class designed for fetch serve configuraton for IMDB API.
 * Created by dps on 9/12/17.
 */
public class ConfigurationFetcher implements Callable<Configuration> {

    private static final String TAG = ConfigurationFetcher.class.getCanonicalName();
    private ConfigurationURLCreator configurationURLCreator;
    private Logger logger;


    public ConfigurationFetcher(ConfigurationURLCreator configurationURLCreator, Logger logger) {
        this.configurationURLCreator = configurationURLCreator;

        this.logger = logger;
    }

    /**
     * Should be use for fetch server API configuration object.
     *
     * @return - Configuration object
     */
    public Configuration fetchConfiguration() throws IOException {
        URL url = configurationURLCreator.createConfigurationURL();
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
        connection.setReadTimeout((int) TimeUnit.SECONDS.toMillis(15));
        try {
            connection.connect();
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStream in = new BufferedInputStream(connection.getInputStream());
                JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
                return new Configuration(reader);
            } else {
                throw new RuntimeException("Erorr Request");
            }
        } catch (IOException e) {
            logger.error(TAG, e.getMessage());
            throw e;
        } finally {
            connection.disconnect();
        }
    }

    @Override
    public Configuration call() throws Exception {
        return fetchConfiguration();
    }
}
