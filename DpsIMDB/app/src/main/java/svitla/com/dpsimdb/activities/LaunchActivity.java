package svitla.com.dpsimdb.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import svitla.com.dpsimdb.IMDBApplication;
import svitla.com.dpsimdb.R;
import svitla.com.dpsimdb.interfaces.Callback;
import svitla.com.dpsimdb.interfaces.Cancelable;
import svitla.com.dpsimdb.interfaces.Logger;
import svitla.com.dpsimdb.network.ConfigurationFetcher;
import svitla.com.dpsimdb.network.ConfigurationURLCreator;
import svitla.com.dpsimdb.internals.Navigator;
import svitla.com.dpsimdb.views.ProgressIndicatorDispatcher;
import svitla.com.dpsimdb.models.Configuration;

public class LaunchActivity extends AppCompatActivity {

    private ProgressIndicatorDispatcher indicator;
    private Logger logger = IMDBApplication.logger();
    private Cancelable cancelable;
    private Navigator navigator;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
        setTitle(getString(R.string.loading_title));
        indicator = new ProgressIndicatorDispatcher(this);
        navigator = new Navigator(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        indicator.showProgressIndicator();
        cancelable = IMDBApplication.serialExecutor().enqueue(new ConfigurationFetcher(new ConfigurationURLCreator(logger), logger), new Callback<Configuration>() {
            @Override
            public void onSuccess(Configuration result) {
                IMDBApplication.saveConfiguration(result);
                logger.debug("TAG", result.toString());
                navigator.showDiscoverScreen();
                indicator.hideProgressIndicator();
            }

            @Override
            public void onError(Throwable error) {
                logger.debug("TAG", error.getMessage());
                indicator.hideProgressIndicator();
                Toast.makeText(LaunchActivity.this, "Sorry but there is an errors while loading configuraion", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        cancelable.cancel();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        indicator.release();
    }
}
