package svitla.com.dpsimdb.internals;

/**
 * Common class for constants.
 * <p>
 * Created by dps on 9/12/17.
 */
public class Constants {
    public static final String CONFIGURATION_PARCELABLE_KEY = "CONFIGURATION.PARCELABLE.KEY";
    public static final String MOVIE_PARCELABLE_KEY = "MOVIE.PARCELABLE.KEY";


    public static final String BASE_URL = "https://api.themoviedb.org/3";

    public static final String API_KEY = "api_key";


}
