package svitla.com.dpsimdb.internals;

import svitla.com.dpsimdb.models.Configuration;

/**
 * Class goal is to detect most suitable image size for loading image based on configuration
 * and real image size.
 * <p>
 * Created by dps on 9/13/17.
 */
public class SizeAdjustable {
    /**
     * Might be use to detect most suitable size of images, based on configuration.
     *
     * @param configuration - Configuration object
     * @param currentSize   - image real size
     * @return - size in acceptable API format.
     */
    public String detectSuitableSize(Configuration configuration, int currentSize) {
        int toFind = Integer.MAX_VALUE;
        String result = "original";
        for (String serverSizes : configuration.supportedPosterSizes) {
            if (serverSizes.startsWith("w")) {
                int parsed = Integer.parseInt(serverSizes.substring(1));
                int absDifference = Math.abs(parsed - currentSize);
                if (toFind > absDifference) {
                    result = serverSizes;
                    toFind = absDifference;
                }
            }
        }
        return result;
    }

}
