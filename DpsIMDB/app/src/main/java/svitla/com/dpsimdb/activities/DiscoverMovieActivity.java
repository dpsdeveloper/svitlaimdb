package svitla.com.dpsimdb.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.List;

import svitla.com.dpsimdb.IMDBApplication;
import svitla.com.dpsimdb.R;
import svitla.com.dpsimdb.interfaces.Callback;
import svitla.com.dpsimdb.interfaces.Cancelable;
import svitla.com.dpsimdb.interfaces.Logger;
import svitla.com.dpsimdb.interfaces.ProgressIndicator;
import svitla.com.dpsimdb.internals.ImdbApiDateFormatter;
import svitla.com.dpsimdb.internals.Navigator;
import svitla.com.dpsimdb.internals.SizeAdjustable;
import svitla.com.dpsimdb.models.Configuration;
import svitla.com.dpsimdb.models.Movie;
import svitla.com.dpsimdb.network.DiscoverFetcher;
import svitla.com.dpsimdb.network.DiscoverUrlCreator;
import svitla.com.dpsimdb.network.MoviesParser;
import svitla.com.dpsimdb.views.ProgressIndicatorDispatcher;
import svitla.com.dpsimdb.views.RecyclerAdapter;

public class DiscoverMovieActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private Cancelable cancelable;
    private Logger logger = IMDBApplication.logger();
    private Configuration configuration;
    private SizeAdjustable adjustable;
    private ProgressIndicator indicator;
    private Navigator navigator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discover_movie);
        setTitle(getString(R.string.discover_title));
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        configuration = IMDBApplication.configuration();
        adjustable = new SizeAdjustable();
        indicator = new ProgressIndicatorDispatcher(this);
        navigator = new Navigator(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        indicator.showProgressIndicator();
        cancelable = IMDBApplication.serialExecutor().enqueue(new DiscoverFetcher(new DiscoverUrlCreator(logger, new ImdbApiDateFormatter()), new MoviesParser()), new Callback<List<Movie>>() {
            @Override
            public void onSuccess(List<Movie> result) {
                indicator.hideProgressIndicator();
                recyclerView.swapAdapter(new RecyclerAdapter(result, navigator), false);
            }

            @Override
            public void onError(Throwable error) {
                indicator.hideProgressIndicator();
                Toast.makeText(DiscoverMovieActivity.this, "Errors occured while loading movies from server", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        cancelable.cancel();
    }
}
