package svitla.com.dpsimdb.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.JsonReader;
import android.util.JsonToken;

import java.io.IOException;

/**
 * POJO class represent "Movie" business object.
 * <p>
 * Created by dps on 9/12/17.
 */
public class Movie implements Parcelable {

    private static final String TITLE = "title";
    private static final String RELEASE_DATE = "release_date";
    private static final String POPULARITY = "popularity";
    private static final String VOTE_COUNT = "vote_count";
    private static final String POSTER_PATH = "poster_path";
    private static final String OVERVIEW = "overview";
    private static final String ID = "id";


    public final String title;
    public final long id;
    public final String releaseDate;
    public final double popularity;
    public final long voteCount;
    public final String posterPath;
    public final String overview;

    public Movie(String title, long id, String releaseDate, double popularity, long voteCount, String posterPath, String overview) {
        this.title = title;
        this.id = id;
        this.releaseDate = releaseDate;
        this.popularity = popularity;
        this.voteCount = voteCount;
        this.posterPath = posterPath;
        this.overview = overview;
    }

    public Movie(JsonReader reader) throws IOException {
        String title = "";
        long id = -1;
        String releaseDate = "";
        double popularity = 0;
        long voteCount = 0;
        String posterPath = "";
        String overview = "";
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            switch (name) {
                case TITLE:
                    if (reader.peek() != JsonToken.NULL) {
                        title = reader.nextString();
                    } else {
                        reader.skipValue();
                    }
                    break;

                case RELEASE_DATE:
                    if (reader.peek() != JsonToken.NULL) {
                        releaseDate = reader.nextString();
                    } else {
                        reader.skipValue();
                    }
                    break;
                case POPULARITY:
                    if (reader.peek() != JsonToken.NULL) {
                        popularity = reader.nextDouble();
                    } else {
                        reader.skipValue();
                    }
                    break;
                case VOTE_COUNT:
                    if (reader.peek() != JsonToken.NULL) {
                        voteCount = reader.nextLong();
                    } else {
                        reader.skipValue();
                    }
                    break;
                case POSTER_PATH:
                    if (reader.peek() != JsonToken.NULL) {
                        posterPath = reader.nextString();
                    } else {
                        reader.skipValue();
                    }
                    break;
                case ID:
                    if (reader.peek() != JsonToken.NULL) {
                        id = reader.nextLong();
                    } else {
                        reader.skipValue();
                    }
                    break;
                case OVERVIEW:
                    if (reader.peek() != JsonToken.NULL) {
                        overview = reader.nextString();
                    } else {
                        reader.skipValue();
                    }
                    break;
                default:
                    reader.skipValue();
                    break;
            }

        }
        reader.endObject();
        this.title = title;
        this.id = id;
        this.releaseDate = releaseDate;
        this.popularity = popularity;
        this.voteCount = voteCount;
        this.posterPath = posterPath;
        this.overview = overview;
    }

    protected Movie(Parcel in) {
        title = in.readString();
        releaseDate = in.readString();
        popularity = in.readDouble();
        voteCount = in.readLong();
        posterPath = in.readString();
        id = in.readLong();
        overview = in.readString();
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    /**
     * {@inheritDoc}
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(releaseDate);
        dest.writeDouble(popularity);
        dest.writeLong(voteCount);
        dest.writeString(posterPath);
        dest.writeLong(id);
        dest.writeString(overview);
    }

    /**
     * Copy method
     */
    public static Movie patchWithBaseUrl(Movie old, String imageBaseUrl) {
        return new Movie(old.title, old.id, old.releaseDate, old.popularity, old.voteCount, old.posterPath.isEmpty() ? "" : imageBaseUrl + old.posterPath, old.overview);
    }
}
