package svitla.com.dpsimdb.views;

import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import svitla.com.dpsimdb.R;
import svitla.com.dpsimdb.interfaces.ProgressIndicator;
import svitla.com.dpsimdb.interfaces.Releasable;

/**
 * Class designed as reusable component for manage Progress indicator view.
 * <p>
 * Created by dps on 9/12/17.
 */
public class ProgressIndicatorDispatcher implements ProgressIndicator, Releasable {
    @Nullable
    private ViewGroup progressView;
    private AppCompatActivity activity;

    public ProgressIndicatorDispatcher(AppCompatActivity activity) {

        this.activity = activity;
    }


    @Override
    public void showProgressIndicator() {
        ensureProgressView();
        if (!isProgressShowing()) {
            ((ViewGroup) activity.getWindow().getDecorView()).addView(progressView);
        }

    }

    private void ensureProgressView() {
        if (progressView == null) {
            initProgress();
        }
    }

    @Override
    public void hideProgressIndicator() {
        ensureProgressView();
        if (isProgressShowing()) {
            ((ViewGroup) activity.getWindow().getDecorView()).removeView(progressView);
        }
    }

    @Override
    public boolean isProgressShowing() {
        return progressView != null && progressView.isAttachedToWindow();
    }

    private void initProgress() {
        progressView = (ViewGroup) activity.getLayoutInflater().inflate(R.layout.progress_layout, (ViewGroup) activity.getWindow().getDecorView(), false);
        progressView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // for disable all touch events over progress view.
                // simple consume all
                return true;
            }
        });
    }

    @Override
    public void release() {
        activity = null;
    }
}
