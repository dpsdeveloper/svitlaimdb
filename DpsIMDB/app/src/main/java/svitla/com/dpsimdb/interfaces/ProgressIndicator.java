package svitla.com.dpsimdb.interfaces;

/**
 * Interface designed for managing progress indicator.
 * <p>
 * Created by dps on 9/12/17.
 */
public interface ProgressIndicator {

    void showProgressIndicator();

    void hideProgressIndicator();

    boolean isProgressShowing();
}
