package svitla.com.dpsimdb.network;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.Callable;

/**
 * Class designed as simple bitmap fetcher. It don't use any caching strategies,
 * just always loading data from network.
 * <p>
 * It might be improved by using some Cache or simple by using Glide or Picasso library :-)
 * <p>
 * Created by dps on 9/13/17.
 */
public class BitmapFetcher implements Callable<Bitmap> {

    private String url;

    public BitmapFetcher(String url) {

        this.url = url;
    }

    @Override
    public Bitmap call() throws Exception {

        URL url = new URL(this.url);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        try {
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);
        } finally {
            connection.disconnect();
        }
    }
}
