package svitla.com.dpsimdb.network;

import android.util.JsonReader;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HttpsURLConnection;

import svitla.com.dpsimdb.models.Movie;

/**
 * Class goal is encapsulate process of fetching data from IMDb server.
 * <p>
 * Created by dps on 9/12/17.
 */
public class DiscoverFetcher implements Callable<List<Movie>> {
    private static final String TAG = DiscoverFetcher.class.getCanonicalName();

    private DiscoverUrlCreator creator;
    private MoviesParser parser;

    public DiscoverFetcher(DiscoverUrlCreator creator, MoviesParser parser) {
        this.creator = creator;
        this.parser = parser;
    }

    /**
     * Might be use to fetch upcoming movies for the date object.
     *
     * @param date - Date object.
     * @return - List of Movies of throws Runtime exception.
     */
    public List<Movie> fetchMoviesForDate(Date date) throws IOException {
        URL url = creator.createCurrentlyShowingDiscoverURL(date);
        HttpURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
        urlConnection.setReadTimeout((int) TimeUnit.SECONDS.toMillis(15));
        try {
            urlConnection.connect();
            if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
                return parser.parse(reader);
            } else {
                throw new RuntimeException("Error executing network request");
            }
        } finally {
            urlConnection.disconnect();
        }
    }

    @Override
    public List<Movie> call() throws Exception {
        // will execute call for fetch upcoming events
        Calendar calendar = Calendar.getInstance();
        // 2017-9-10
        calendar.set(2017, 8, 9);
        return fetchMoviesForDate(calendar.getTime());
    }
}
