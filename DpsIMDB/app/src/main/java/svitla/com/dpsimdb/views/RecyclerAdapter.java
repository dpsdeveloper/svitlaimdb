package svitla.com.dpsimdb.views;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import svitla.com.dpsimdb.R;
import svitla.com.dpsimdb.internals.Navigator;
import svitla.com.dpsimdb.models.Movie;

/**
 * Simple Recycler Adapter.
 * <p>
 * Created by dps on 9/13/17.
 */
public class RecyclerAdapter extends android.support.v7.widget.RecyclerView.Adapter<MovieViewHolder> {

    private List<Movie> dataSet;
    private Navigator navigator;

    public RecyclerAdapter(List<Movie> dataSet, Navigator navigator) {

        this.dataSet = dataSet;
        this.navigator = navigator;
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_movie_layout, parent, false);
        return new MovieViewHolder(view, navigator);
    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, int position) {
        holder.bind(dataSet.get(holder.getAdapterPosition()));
    }

    @Override
    public void onViewRecycled(MovieViewHolder holder) {
        super.onViewRecycled(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}
