package svitla.com.dpsimdb.network;

import android.util.JsonReader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import svitla.com.dpsimdb.models.Movie;

/**
 * Class designed for parse server response.
 * <p>
 * Created by dps on 9/12/17.
 */
public class MoviesParser {

    private static final String RESULTS = "results";
    public static final int INITIAL_CAPACITY = 20;

    /**
     * Might be use for parse server response.
     *
     * @param reader - JSON reader object.
     * @return - list of parsed Movies
     * @throws IOException - if parsing failed
     */
    public List<Movie> parse(JsonReader reader) throws IOException {
        List<Movie> result = Collections.emptyList();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (RESULTS.equalsIgnoreCase(name)) {
                result = new ArrayList<>(INITIAL_CAPACITY);
                reader.beginArray();
                while (reader.hasNext()) {
                    result.add(new Movie(reader));
                }
                reader.endArray();
                // safe collection wrapping
                result = Collections.unmodifiableList(result);
            } else {
                reader.skipValue();
            }
        }
        return result;
    }

}
