package svitla.com.dpsimdb.interfaces;

/**
 * Callback interface, designed for get result of the execution.
 * <p>
 * Created by dps on 9/12/17.
 */
public interface Callback<T> {

    void onSuccess(T result);

    void onError(Throwable error);

}
