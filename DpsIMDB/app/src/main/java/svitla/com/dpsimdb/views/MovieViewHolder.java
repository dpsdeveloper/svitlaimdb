package svitla.com.dpsimdb.views;

import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import svitla.com.dpsimdb.IMDBApplication;
import svitla.com.dpsimdb.R;
import svitla.com.dpsimdb.interfaces.Callback;
import svitla.com.dpsimdb.interfaces.Cancelable;
import svitla.com.dpsimdb.internals.Navigator;
import svitla.com.dpsimdb.internals.SizeAdjustable;
import svitla.com.dpsimdb.models.Movie;
import svitla.com.dpsimdb.network.BitmapFetcher;

/**
 * Class view holder for representing UI.
 * <p>
 * Created by dps on 9/13/17.
 */
public class MovieViewHolder extends RecyclerView.ViewHolder {

    private ImageView placeHolder;
    private TextView title;
    private TextView releaseDate;
    private TextView popularity;
    private TextView votes;
    private final Navigator navigator;
    private Cancelable cancelable;
    private static String imageSizePath = "";

    public MovieViewHolder(View itemView, Navigator navigator) {
        super(itemView);
        placeHolder = (ImageView) itemView.findViewById(R.id.place_holder);
        title = (TextView) itemView.findViewById(R.id.title);
        releaseDate = (TextView) itemView.findViewById(R.id.release_date);
        popularity = (TextView) itemView.findViewById(R.id.popularity);
        votes = (TextView) itemView.findViewById(R.id.vote_count);
        this.navigator = navigator;
    }

    /**
     * Might be called to bind data to it's view.
     *
     * @param model - Item model
     */
    void bind(final Movie model) {
        if (imageSizePath.isEmpty()) {
            // will execute detection once for all holders.
            imageSizePath = new SizeAdjustable().detectSuitableSize(IMDBApplication.configuration(), itemView.getResources().getDimensionPixelSize(R.dimen.image_cover_size));
        }
        if (!model.posterPath.isEmpty()) {
            String finalURL = IMDBApplication.configuration().baseURL + imageSizePath + model.posterPath;
            cancelable = IMDBApplication.poolExecutor().enqueue(new BitmapFetcher(finalURL), new Callback<Bitmap>() {
                @Override
                public void onSuccess(Bitmap result) {
                    placeHolder.setImageBitmap(result);
                }

                @Override
                public void onError(Throwable error) {

                }
            });
        }
        title.setText(model.title);
        releaseDate.setText(releaseDate.getResources().getString(R.string.release_date, model.releaseDate));
        popularity.setText(popularity.getResources().getString(R.string.popularity, model.popularity));
        votes.setText(votes.getResources().getString(R.string.votes, model.voteCount));
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigator.showDetailsScreen(model);
            }
        });
    }

    /**
     * Should be called for unbind from viewholder.
     */
    void unbind() {
        title.setText("");
        releaseDate.setText("");
        popularity.setText("");
        votes.setText("");
        placeHolder.setImageDrawable(itemView.getContext().getDrawable(R.drawable.nocover));
        itemView.setOnClickListener(null);
        if (cancelable != null) {
            cancelable.cancel();
        }
    }

}
