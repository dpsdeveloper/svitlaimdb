package svitla.com.dpsimdb.interfaces;

/**
 * Interface designed for encapsulate resource releasing.
 * <p>
 * Created by dps on 9/12/17.
 */
public interface Releasable {
    /**
     * Should be use for release different resources.
     */
    void release();

}
