package svitla.com.dpsimdb.interfaces;

/**
 * Interface designed as an substitution of {@link java.util.concurrent.Future} cancellation.
 * <p>
 * Created by dps on 9/12/17.
 */
public interface Cancelable {
    /**
     * Might be use to cancel some work.
     */
    void cancel();

}
