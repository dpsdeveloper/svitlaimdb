package svitla.com.dpsimdb.internals;

import android.os.Handler;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import svitla.com.dpsimdb.IMDBApplication;
import svitla.com.dpsimdb.interfaces.Callback;
import svitla.com.dpsimdb.interfaces.Cancelable;

/**
 * Class designed for facilitate http requests execution.
 * Clients should use {@link this#enqueue(Callable, Callback)} to schedule execution.
 * Result will be delivered on the main thread.
 * <p>
 * Created by dps on 9/12/17.
 */
public class RequestExecutor {
    public static final String TAG = RequestExecutor.class.getCanonicalName();
    private ExecutorService service;
    private Handler handler;

    public RequestExecutor(ExecutorService service, Handler handler) {
        this.service = service;
        this.handler = handler;
    }

    /**
     * Should be use for schedule some work.
     *
     * @param callable - Generic Callable object.
     * @param callback - Generic callback object
     * @param <T>      - generic param
     * @return - Cancelable object
     */
    public <T> Cancelable enqueue(final Callable<T> callable, final Callback<T> callback) {
        final Future future = service.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    final T result = callable.call();
                    if (canDeliverResult()) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onSuccess(result);
                            }
                        });
                    }
                } catch (final Exception ex) {
                    if (canDeliverResult()) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onError(ex);
                            }
                        });
                    }
                }
            }

            private boolean canDeliverResult() {
                boolean canDeliverResult = !Thread.currentThread().isInterrupted();
                if (!canDeliverResult) {
                    IMDBApplication.logger().debug(TAG, "Result will be ignored");
                }
                return canDeliverResult;
            }
        });
        return new Cancelable() {
            @Override
            public void cancel() {
                future.cancel(true);
            }
        };
    }


}
